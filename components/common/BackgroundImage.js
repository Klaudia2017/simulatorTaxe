import React from 'react';
import { View, ImageBackground } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const BackgroundImage = ({ resizeMode, opacity, source, children }) => {
	const { container, image } = styles;

	return (
   
		<ImageBackground   
		source={source} 
		style={{width: '100%', height: '100%'}}	
		>
			<View style={styles.overlay}>
			{children}
			</View>
		</ImageBackground>

	);
};

BackgroundImage.propTypes = {
	resizeMode: PropTypes.string.isRequired,
	source: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	])
};

const styles = {
	overlay: {
		flex: 1,
		justifyContent: 'center',  
		backgroundColor: 'rgba(255,255,255,0.8)',
    }
};

export {BackgroundImage};

