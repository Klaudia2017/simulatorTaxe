
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const Header = ({ headerTitle }) => {
   
	return (
		<View style={styles.viewStyle}>
			<Text style={styles.text}>{headerTitle}</Text>
		</View>
	);

};

Header.propTypes = {
	headerTitle: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
	text: {
		fontSize: 20, 
		color: APP_COLORS.primary,
	},
	viewStyle: {
		backgroundColor: APP_COLORS.darkPrimary || 'gray',
		justifyContent: 'center',
		alignItems: 'center',
		height: 80,
		paddingTop: 35,
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 4},
		shadowOpacity: 0.2,
		elevation: 4,
		position: 'relative'
	}
});

export  {Header};
