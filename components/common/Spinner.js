
import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import {APP_COLORS} from '../../style/color';
import PropTypes from 'prop-types';

const Spinner =  ({size, color, loading}) => {
    
	return (
		<View style={styles.container}>
			<ActivityIndicator
				size={size || 'large'}
				color={color|| APP_COLORS.primaryAction }
				animating={loading} 
			/>
		</View>
	);

};

Spinner.propTypes = {
	size: PropTypes.number,
	color: PropTypes.string,
	loading: PropTypes.bool
};


const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: APP_COLORS.primaryAction || 'gray',
	},
});


export {Spinner};
