
import React, { Component } from 'react';
import { View, Text, StyleSheet, LayoutAnimation} from 'react-native';
import { ButtonForm, Card, CardSection, InputForm, BackgroundImage  } from './common';
import { APP_COLORS } from '../style/color';

// Animation réglage - Spring 
var CustomLayoutSpring = {
    duration: 400,
    create: {
      type: LayoutAnimation.Types.spring,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 0.2,
    },
    update: {
      type: LayoutAnimation.Types.spring,
      springDamping: 0.2,
    },
  };


export class MainView extends Component {

    constructor(props) {
        super(props)  
        this.state = {

            salaireAnnuelBrut: 0,
            salaireAnnuelNet: 0,
            salaireAnnuelNetApresImpot: 0,

            salaireMensuelBrut: 0,
            salaireMensuelNet: 0,

            impotAnnuel: 0,
            impotMensuel : 0,   
           
            error: '',
            calculOption1: true,
            calculOption2: false, 
            showResult: false,
            symboleMonnaie : ' €', 

        }
    }  

     // Afficher l'animation lorsque l'état a changé
    componentWillUpdate(nextProps, nextState) {
        LayoutAnimation.spring();
    }

    inputChange = (text) => {
        this.setState({inputValue: text});
    }

    onFocuscalculOption1 = () => {
        this.setState({
            error: '',
            salaireAnnuelNet : 0,
            salaireAnnuelBrut : '',
            salaireMensuelNet : 0,
            impotAnnuel : 0,
            impotMensuel : 0,
            salaireAnnuelNetApresImpot : 0,
        });
    }

    onFocuscalculOption2 = () =>{
        this.setState({
            error: '',
            salaireAnnuelNet : 0,
            salaireMensuelNet : '',
            salaireAnnuelBrut : 0,
            impotAnnuel : 0,
            impotMensuel : 0,
            salaireAnnuelNetApresImpot : 0,
        });
    }

    onButtonPress = () =>  {
            
        //Calcul d'impot à partir du salaire annuel brut
        if(this.state.calculOption1) {
            
            let salaireAnnuelBrut = Number(this.state.salaireAnnuelBrut);
            if(salaireAnnuelBrut >= 0) {

            // Calcul  Input Salaire net annuels
            let salaireAnnuelNet = Math.round(salaireAnnuelBrut * 0.75);
            
            // Calcul Input Impot Annuel
            let impotAnnuel = this.calculerImpot(salaireAnnuelNet);

            // Afficher l'animation lorsque l'état a changé
            //LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

            // Verification des données saisies
            // Vider Notification 
            // Mettre à jour Input Salaire net annuels
            // Mettre à jour Input Salaire net mensuel
            // Mettre à jour Input Impot Annuel
            // Mettre à jour Input Impot Mensuel
            this.setState({
                showResult: true, 
                error: '',
                salaireAnnuelNet : salaireAnnuelNet,
                salaireMensuelNet : Math.round(salaireAnnuelNet/12),
                impotAnnuel : impotAnnuel,
                impotMensuel : Math.round(impotAnnuel/12),
                salaireAnnuelNetApresImpot : Math.round(Math.round(salaireAnnuelNet/12) - (Math.round(impotAnnuel/12)) )
                });

            } else {
                this.setState({
                    showResult: false, 
                    error: 'Le montant doit etre positive',
                    salaireAnnuelNet : 0,
                    salaireMensuelNet : 0,
                    impotAnnuel : 0,
                    impotMensuel : 0,
                    salaireAnnuelNetApresImpot : 0,

                });
            }   
            
        //calcule d'impot à partir du salaire mensuel net
        } else {

                let salaireMensuelNet = Number(this.state.salaireMensuelNet);
            if(salaireMensuelNet >= 0) {

            // Calcul  Input Salaire net annuels
            let salaireAnnuelNet = Math.round(salaireMensuelNet * 12);
            
            // Calcul  Input Salaire annuels brut 
            let salaireAnnuelBrut = Math.round(salaireAnnuelNet / 0.75);

            // Calcul Input Impot Annuel
            let impotAnnuel = this.calculerImpot(salaireAnnuelNet);
            
            // Afficher l'animation lorsque l'état a changé
            //LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
            
            // Verification des données saisies
            // Vider Notification 
            // Mettre à jour Input Salaire net annuels
            // Mettre à jour Input Salaire net mensuel
            // Mettre à jour Input Impot Annuel
            // Mettre à jour Input Impot Mensuel
            this.setState({
                showResult: true, 
                error: '',
                salaireAnnuelNet : salaireAnnuelNet,
                salaireAnnuelBrut : salaireAnnuelBrut,
                impotAnnuel : impotAnnuel,
                impotMensuel : Math.round(impotAnnuel/12),
                salaireAnnuelNetApresImpot : Math.round(salaireMensuelNet - (Math.round(impotAnnuel/12)))
                });

            } else {
            this.setState({
                showResult: false, 
                error: 'Le montant doit etre positive',
                salaireAnnuelNet : 0,
                salaireAnnuelBrut : 0,
                impotAnnuel : 0,
                impotMensuel : 0,
                salaireAnnuelNetApresImpot : 0,
                });
            }  
        } 
    } 
           

    //fais calcul d'impot
    calculerImpot = (salaireAnnuelNet) => {
        let impots = 0;
        if(salaireAnnuelNet < 9807){ 
            impots = 0;
        }else if(salaireAnnuelNet > 9807 && salaireAnnuelNet < 27086){
            impots =  (salaireAnnuelNet - 9807) * 0.14;   
        }else if(salaireAnnuelNet > 27086 && salaireAnnuelNet < 72617){
            impots =  ((27086 - 9807) * 0.14) + ((salaireAnnuelNet - 27086) * 0.30);
        } else if(salaireAnnuelNet > 72617 && salaireAnnuelNet < 153783){
            impots =  ((27086 - 9807) * 0.14) + ((72617 - 27086) * 0.30) + ((salaireAnnuelNet - 72617) * 0.41);
        }else{
            impots =  ((27086 - 9807) * 0.14) + ((72617 - 27086) * 0.30) + ((153783 - 72617) * 0.41) + ((salaireAnnuelNet - 153783) * 0.45);
        }
        return Math.round(impots);   
    }

    //change le champe pour saisir des donné
    onChangeChamp = () =>  {
        this.setState({
            calculOption1 : !this.state.calculOption1,
            calculOption2: !this.state.calculOption2,
            salaireMensuelNet: '',
            salaireAnnuelNet : 0,
            salaireAnnuelBrut : '',
            impotAnnuel : 0,
            impotMensuel : 0,
            salaireAnnuelNetApresImpot : 0,
            showResult: false, 
             });
    }
    //affiche le resultat
    
     renderResultat = () =>  {
            if(this.state.showResult){
                return ( 
                    <View>
                        <CardSection >   
                            <InputForm     
                            editable={false} 
                            placeholder=""
                            label="Salaire annuel net"
                            value={`${this.state.salaireAnnuelNet} `}
                            symboleMonnaie = {this.state.symboleMonnaie}
                            />
                        </CardSection>
                        <CardSection >   
                            <InputForm  
                            editable={false} 
                            placeholder=""
                            label="Montant total des impots"    
                            value={`${this.state.impotAnnuel}`}
                            symboleMonnaie = {this.state.symboleMonnaie}
                            /> 
                        </CardSection>
                      
                        <CardSection  >   
                            <InputForm  
                            editable={false} 
                            placeholder=""
                            label="Impot mensuel"
                            value={`${this.state.impotMensuel}`}
                            symboleMonnaie = {this.state.symboleMonnaie}
                            />        
                        </CardSection>

                        <CardSection 
                        last={true}
                        >   
                            <InputForm  
                            editable={false} 
                            placeholder=""
                            label="Salaire mensuel Vr. net"
                            value={`${this.state.salaireAnnuelNetApresImpot}`}
                            symboleMonnaie = {this.state.symboleMonnaie}
                            />        
                        </CardSection>
                    </View>
                );
                
            }
    }

    render() {
        const {container, textContainer, text, errorTextStyle, importantText, labelCadre}  = styles;
        const {salaireAnnuelBrut, salaireMensuelNet} = this.state;

        return (
            <View style={container}>
                <BackgroundImage
                    resizeMode="cover"      
                    source={require('../style/background.jpg')}
                >
                    <Card>
                        <CardSection>
                            <InputForm  
                                onFocus={() => this.onFocuscalculOption1()}
                                editable={this.state.calculOption1} 
                                placeholder="0"
                                label="Salaire annuel brut"
                                value={`${salaireAnnuelBrut}`}
                                onChangeText={salaireAnnuelBrut => this.setState({ salaireAnnuelBrut })}
                                active={this.state.calculOption1}
                                symboleMonnaie = {this.state.symboleMonnaie}
                            />
                        </CardSection>
                        <CardSection>
                            <InputForm  
                            onFocus={() => this.onFocuscalculOption2()}
                            editable={this.state.calculOption2} 
                            placeholder="0"
                            label="Salaire mensuel net"
                            value={`${salaireMensuelNet}`}
                            onChangeText={(salaireMensuelNet) => {this.setState({ salaireMensuelNet }) }}
                            active={this.state.calculOption2}
                            symboleMonnaie = {this.state.symboleMonnaie}
                            
                            />
                        </CardSection>

                        <View style={textContainer}>
                            <Text style={errorTextStyle}>
                                {this.state.error}
                            </Text>
                        </View>
                        <CardSection   
                        last={true}
                        >
                            <ButtonForm 
                            onPress={ () => this.onChangeChamp() }
                            backgroundColor ={'#8285d6'}
                            >
                            BASCULEZ
                            </ButtonForm>  
                        
                            <ButtonForm 
                            onPress={ () => this.onButtonPress() }
                            backgroundColor ={'red'}
                            >
                            CALCULEZ
                            </ButtonForm>
                            
                        </CardSection>
                        {this.renderResultat()}
                    </Card>
                </BackgroundImage>
            </View>
        );     
    }
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: APP_COLORS.primary,
    },  
    textContainer: {
        paddingTop: 8,
        paddingLeft: 8,
        margin: 0,
    },
    errorTextStyle: {
        fontSize: 16,
        alignSelf: 'center',
        color: APP_COLORS.accent,
    },
    text: {
        color: APP_COLORS.secondaryText
    },
    importantText: {
        color: APP_COLORS.primaryAction
    },
    labelCadre: {
        alignItems: 'center', 
        paddingHorizontal: '3%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: APP_COLORS.secondaryText,  
        alignSelf: 'stretch',
        backgroundColor: APP_COLORS.primary,
    }
});
